import { useState } from 'react';
import './App.css'

function App() {
 
  const[number,setnumber]= useState(1)
 
  function multiply(){
    setnumber(number * 5);
  }

  return (
    <>
      <header></header>
      <main>
        <h1>{number}</h1>
        <button onClick={multiply}> Multiply by 5</button>
      </main>
      <footer></footer>
    </>
  )
}

export default App
